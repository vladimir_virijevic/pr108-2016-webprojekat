﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public double Grade { get; set; }
        public bool IsDeleted { get; set; } = false;
        public bool IsAllowed { get; set; } = false;
        public int ApartmentId { get; set; }
        public int GuestId { get; set; }
        public string GuestUsername { get; set; }
        public string HostUsername { get; set; }
        public string ApartmentAddress { get; set; }
    }
}