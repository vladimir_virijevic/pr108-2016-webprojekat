﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public abstract class User
    {
        public abstract int Id { get; set; }
        public abstract string Username { get; set; }
        public abstract string Password { get; set; }
        public abstract string Name { get; set; }
        public abstract string LastName { get; set; }
        public abstract string Gender { get; set; }
        public abstract string Type { get; set; }
        public abstract bool IsDeleted { get; set; }
    }
}