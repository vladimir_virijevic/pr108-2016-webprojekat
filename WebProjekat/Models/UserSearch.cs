﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class UserSearch
    {
        public string Role { get; set; }
        public string Gender { get; set; }
        public string Username { get; set; }
    }
}