﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Apartment
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int RoomNumber { get; set; }
        public int GuestCapacity { get; set; }
        public string Dates { get; set; }
        public string DatesAvailability { get; set; }

        public string Pictures { get; set; }
        public double Price { get; set; }
        public string CheckInTime { get; set; } = "2 AM";
        public string CheckOutTime { get; set; } = "10 PM";
        public string ApartmentStatus { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string Amenities { get; set; }


        // Navigation
        public string Host { get; set; }

        public string Guest { get; set; }

        public string Comments { get; set; }

        public string Reservations { get; set; }


        public Location Location { get; set; }
    }
}