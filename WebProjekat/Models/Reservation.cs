﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public string StartDate { get; set; }
        public int Days { get; set; } = 1;
        public double TotalPrice { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string Status { get; set; } = "";
        public string GuestUsername { get; set; }
        public string HostUsername { get; set; }
        public int ApartmentId { get; set; }
    }
}