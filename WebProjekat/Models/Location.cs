﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Location
    {
        public int Id { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }

        // Navigation
        public string Address { get; set; }
        public string StreetNumber { get; set; }
        public string PostalCode { get; set; }
        public int ApartmentId { get; set; }
    }
}