﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class ApartmentSearch
    {

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Location { get; set; }
        public double MinPrice { get; set; }
        public double MaxPrice { get; set; }
        public int MinRoomNumber { get; set; }
        public int MaxRoomNumber { get; set; }
        public int GuestCapacity { get; set; }
    }
}