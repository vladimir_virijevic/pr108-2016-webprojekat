﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class Host : User
    {
        public override int Id { get; set; }
        public override string Username { get; set; }
        public override string Password { get; set; }
        public override string Name { get; set; }
        public override string LastName { get; set; }
        public override string Gender { get; set; }
        public override string Type { get; set; } = "HOST";
        public override bool IsDeleted { get; set; } = false;
        public string ApartmentsForRent;
    }
}