﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.Models
{
    public class ApartmentInput
    {
        public string Type { get; set; }
        public string Pictures { get; set; }
        public int RoomNumber { get; set; }
        public int GuestCapacity { get; set; }
        public string Dates { get; set; }
        public double Price { get; set; }
        public string CheckInTime { get; set; } = "2 AM";
        public string CheckOutTime { get; set; } = "10 PM";
        public string ApartmentStatus { get; set; } = "INACTIVE";
        public IEnumerable<string> Amenities { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; } = "0";
        public string Longitude { get; set; } = "0";
        public string StreetNumber { get; set; }
        public string PostalCode { get; set; }
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
        public int Id { get; set; }
        public string DatesAvailability { get; set; }
        public string Host { get; set; }
        public string Guest { get; set; }
        public string Comments { get; set; }
    }
}