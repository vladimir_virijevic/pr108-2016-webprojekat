﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;
using WebProjekat.Repository;

namespace WebProjekat.Security
{
    public class AccountSecurity
    {
        public ApplicationRepository repository = new ApplicationRepository();

        public bool LoginIsValid(string username, string password)
        {
            List<Admin> admins = repository.GetAdmins();

            foreach (Admin admin in admins)
            {
                if (admin.Username == username && admin.Password == password)
                {
                    return true;
                }
            }

            List<Host> hosts = repository.GetHosts();

            foreach (Host host in hosts)
            {
                if (host.Username == username && host.Password == password)
                {
                    return true;
                }
            }

            List<Guest> guests = repository.GetGuests();

            foreach (Guest guest in guests)
            {
                if (guest.Username == username && guest.Password == password)
                {
                    return true;
                }
            }

            return false;
        }
    }
}