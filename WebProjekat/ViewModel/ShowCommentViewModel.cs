﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;

namespace WebProjekat.ViewModel
{
    public class ShowCommentViewModel
    {
        public List<Comment> Comments { get; set; }
        public string UserType { get; set; }
    }
}