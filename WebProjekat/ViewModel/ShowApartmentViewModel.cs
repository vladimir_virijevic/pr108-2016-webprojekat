﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;

namespace WebProjekat.ViewModel
{
    public class ShowApartmentViewModel
    {
        public Apartment Apartment { get; set; }
        public string UserType { get; set; }
        public List<Comment> Comments { get; set; }
    }
}