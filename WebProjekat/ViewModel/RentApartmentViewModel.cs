﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjekat.ViewModel
{
    public class RentApartmentViewModel
    {
        public string AvailableDates { get; set; }
        public string RentDate { get; set; }
        public int Id { get; set; }
        public int Days { get; set; }
    }
}