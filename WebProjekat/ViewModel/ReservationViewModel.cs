﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;

namespace WebProjekat.ViewModel
{
    public class ReservationViewModel
    {
        public List<Reservation> Reservations { get; set; }
        public Guest User { get; set; }
    }
}