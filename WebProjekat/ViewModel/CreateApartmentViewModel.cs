﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;

namespace WebProjekat.ViewModel
{
    public class CreateApartmentViewModel
    {
        public List<Amenity> Amenities { get; set; }
    }
}