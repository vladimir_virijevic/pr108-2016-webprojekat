﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;

namespace WebProjekat.ViewModel
{
    public class HomeViewModel
    {
        public string Username { get; set; }
        public string UserType { get; set; }
        public bool UserIsLoggedIn { get; set; }
        public List<Apartment> Apartments { get; set; }
    }
}