﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjekat.Models;

namespace WebProjekat.ViewModel
{
    public class ShowUserViewModel
    {
        public List<User> Users { get; set; }
        public string AccountType { get; set; }
    }
}