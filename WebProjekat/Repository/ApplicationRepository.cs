﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using WebProjekat.Models;
using WebProjekat.ViewModel;

namespace WebProjekat.Repository
{
    public class ApplicationRepository
    {
        #region Admin
        public Admin GetAdmin(string username)
        {
            var users = GetAdmins();

            foreach (var user in users)
            {
                if (user.Username == username && user.IsDeleted == false)
                {
                    return user;
                }
            }

            return null;
        }



        public List<Admin> GetAdmins()
        {
            List<Admin> admins = new List<Admin>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/admins.txt"));

            foreach (string line in lines)
            {
                string[] adminInfo = line.Split('|');
                Admin adminToAdd = new Admin()
                {
                    Id = int.Parse(adminInfo[0]),
                    Username = adminInfo[1],
                    Password = adminInfo[2],
                    Name = adminInfo[3],
                    LastName = adminInfo[4],
                    Gender = adminInfo[5]
                };

                admins.Add(adminToAdd);
            }

            return admins;
        }
        #endregion

        #region Host
        public Host GetHost(string username)
        {
            var users = GetHosts();

            foreach (var user in users)
            {
                if (user.Username == username && user.IsDeleted == false)
                {
                    return user;
                }
            }

            return null;
        }

        public bool AddHost(Host hostToAdd)
        {
            if (UserExists(hostToAdd.Username))
            {
                return false;
            }

            string username = hostToAdd.Username;
            string password = hostToAdd.Password;
            string name = hostToAdd.Name;
            string lastName = hostToAdd.LastName;
            string gender = hostToAdd.Gender;

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/hosts.txt"));
            int id = SetId(lines);

            using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath(@"~/App_Data/hosts.txt"), true))
            {
                writer.WriteLine($"{id}|{username}|{password}|{name}|{lastName}|{gender}|");
                return true;
            }
        }

        public List<Host> GetHosts()
        {
            List<Host> hosts = new List<Host>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/hosts.txt"));

            foreach (string line in lines)
            {
                string[] hostInfo = line.Split('|');
                Host hostToAdd = new Host()
                {
                    Id = int.Parse(hostInfo[0]),
                    Username = hostInfo[1],
                    Password = hostInfo[2],
                    Name = hostInfo[3],
                    LastName = hostInfo[4],
                    Gender = hostInfo[5]
                };

                hosts.Add(hostToAdd);
            }

            return hosts;
        }
        #endregion

        #region Guest
        public Guest GetGuest(string username )
        {
            var users = GetGuests();

            foreach (var user in users)
            {
                if (user.Username == username && user.IsDeleted == false)
                {
                    return user;
                }
            }

            return null;
        }

        public bool AddGuest(Guest guest)
        {
            if (UserExists(guest.Username))
            {
                return false;
            }

            string username = guest.Username;
            string password = guest.Password;
            string name = guest.Name;
            string lastName = guest.LastName;
            string gender = guest.Gender;

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/guests.txt"));
            int id = SetId(lines);

            using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath(@"~/App_Data/guests.txt"), true))
            {
                writer.WriteLine($"{id}|{username}|{password}|{name}|{lastName}|{gender}|");
                return true;
            }
        }

        public List<Guest> GetGuests()
        {
            List<Guest> guests = new List<Guest>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/guests.txt"));

            foreach (string line in lines)
            {
                string[] guestInfo = line.Split('|');
                Guest guestToAdd = new Guest()
                {
                    Id = int.Parse(guestInfo[0]),
                    Username = guestInfo[1],
                    Password = guestInfo[2],
                    Name = guestInfo[3],
                    LastName = guestInfo[4],
                    Gender = guestInfo[5]
                };

                guests.Add(guestToAdd);
            }

            return guests;
        }
        #endregion

        #region Apartment
        public bool UpdateApartment(Apartment apartment)
        {

            if (apartment != null)
            {
                string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt")).ToArray();

                int targetIndex = -1;

                for (int i = 0; i < lines.Count(); i++)
                {
                    string[] info = lines[i].Split('|');

                    if (int.Parse(info[0]) == apartment.Id)
                    {
                        targetIndex = i;
                        break;
                    }
                }

                lines[targetIndex] = $"{apartment.Id}|{apartment.Host}|{apartment.ApartmentStatus}|{apartment.Type}|{apartment.RoomNumber}|{apartment.GuestCapacity}|{apartment.Dates}|{apartment.DatesAvailability}|{apartment.Price}|{apartment.CheckInTime}|{apartment.CheckOutTime}|{apartment.Amenities}|{apartment.Location.Address}|{apartment.Location.Lat}|{apartment.Location.Long}|{apartment.Location.StreetNumber}|{apartment.Location.PostalCode}|{apartment.Pictures}|{apartment.IsDeleted}";
                File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt"), lines);
            }

            return true;
        }

        public void AddApartment(ApartmentInput apartmentInput)
        {
            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt"));
            int id = SetId(lines);

            // amenities and pictures
            string amenities = string.Empty;
            string pictures = string.Empty;

            foreach (string amenity in apartmentInput.Amenities)
            {
                amenities += amenity + "_";
            }

            amenities = amenities.Remove(amenities.Length - 1);

            foreach (var picture in apartmentInput.Files)
            {
                pictures += picture.FileName + "*";
            }

            pictures = pictures.Remove(pictures.Length - 1);

            string hostUsername = string.Empty;

            if (HttpContext.Current.Session["User"] != null)
            {
                hostUsername = ((Guest)HttpContext.Current.Session["User"]).Username;
            }

            // Postavljaju se datumi
            apartmentInput.Dates = Regex.Replace(apartmentInput.Dates, @"\s+", "");


            using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt"), true))
            {
                writer.WriteLine($"{id}|{hostUsername}|{apartmentInput.ApartmentStatus}|{apartmentInput.Type}|{apartmentInput.RoomNumber}|{apartmentInput.GuestCapacity}|{apartmentInput.Dates}|{apartmentInput.Dates}|{apartmentInput.Price}|{apartmentInput.CheckInTime}|{apartmentInput.CheckOutTime}|{amenities}|{apartmentInput.Address}|{apartmentInput.Latitude}|{apartmentInput.Longitude}|{apartmentInput.StreetNumber}|{apartmentInput.PostalCode}|{pictures}|FALSE");
            }
        }

        public List<Apartment> GetApartments()
        {
            List<Apartment> apartments = new List<Apartment>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt"));

            foreach (string line in lines)
            {
                string[] apartmentInfo = line.Split('|');
                Apartment apartment = new Apartment()
                {
                    Id = int.Parse(apartmentInfo[0]),
                    Type = apartmentInfo[3],
                    RoomNumber = int.Parse(apartmentInfo[4]),
                    GuestCapacity = int.Parse(apartmentInfo[5]),
                    Dates = apartmentInfo[6],
                    DatesAvailability = apartmentInfo[7],
                    Price = double.Parse(apartmentInfo[8]),
                    CheckInTime = apartmentInfo[9],
                    CheckOutTime = apartmentInfo[10],
                    ApartmentStatus = apartmentInfo[2],
                    Amenities = apartmentInfo[11],
                    Location = new Location()
                    {
                        Address = apartmentInfo[12],
                        StreetNumber = apartmentInfo[15],
                        Long = double.Parse(apartmentInfo[13]),
                        Lat = double.Parse(apartmentInfo[14]),
                        PostalCode = apartmentInfo[16]
                    },
                    Host = apartmentInfo[1],
                    Pictures = apartmentInfo[17],
                    IsDeleted = (apartmentInfo[18].ToLower() == "false") ? false : true
                };

                if (!apartment.IsDeleted)
                {
                    apartments.Add(apartment);
                }
            }

            return apartments;
        }

        public Apartment GetApartment(int id)
        {
            Apartment apartment = new Apartment();

            List<Apartment> apartments = GetApartments();

            foreach (var a in apartments)
            {
                if (a.Id == id)
                {
                    return a;
                }
            }

            return null ;
        }

        public bool ActivateApartment(int id)
        {
            Apartment apartmentToActivate = GetApartment(id);

            if (apartmentToActivate != null)
            {
                if (apartmentToActivate.ApartmentStatus.ToLower() == "inactive")
                {
                    string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt")).ToArray();

                    int targetIndex = -1;

                    for (int i = 0; i < lines.Count(); i++)
                    {
                        string[] info = lines[i].Split('|');

                        if (int.Parse(info[0]) == id)
                        {
                            targetIndex = i;
                            break;
                        }
                    }

                    lines[targetIndex] = $"{apartmentToActivate.Id}|{apartmentToActivate.Host}|ACTIVE|{apartmentToActivate.Type}|{apartmentToActivate.RoomNumber}|{apartmentToActivate.GuestCapacity}|{apartmentToActivate.Dates}|{apartmentToActivate.Dates}|{apartmentToActivate.Price}|{apartmentToActivate.CheckInTime}|{apartmentToActivate.CheckOutTime}|{apartmentToActivate.Amenities}|{apartmentToActivate.Location.Address}|{apartmentToActivate.Location.Lat}|{apartmentToActivate.Location.Long}|{apartmentToActivate.Location.StreetNumber}|{apartmentToActivate.Location.PostalCode}|{apartmentToActivate.Pictures}|{apartmentToActivate.IsDeleted}";
                    File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt"), lines);
                }
            }

            return false;
        }

        public bool DeleteApartment(int id)
        {
            Apartment apartment = GetApartment(id);

            if (apartment != null)
            {
                string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt")).ToArray();

                int targetIndex = -1;

                for (int i = 0; i < lines.Count(); i++)
                {
                    string[] info = lines[i].Split('|');

                    if (int.Parse(info[0]) == id)
                    {
                        targetIndex = i;
                        break;
                    }
                }

                lines[targetIndex] = $"{apartment.Id}|{apartment.Host}|{apartment.ApartmentStatus}|{apartment.Type}|{apartment.RoomNumber}|{apartment.GuestCapacity}|{apartment.Dates}|{apartment.Dates}|{apartment.Price}|{apartment.CheckInTime}|{apartment.CheckOutTime}|{apartment.Amenities}|{apartment.Location.Address}|{apartment.Location.Lat}|{apartment.Location.Long}|{apartment.Location.StreetNumber}|{apartment.Location.PostalCode}|{apartment.Pictures}|True";
                File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/apartments.txt"), lines);
                return true;
            }

            return false;
        }

        #endregion

        #region Comment
        public void UpdateComment(Comment item)
        {
            string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/comments.txt")).ToArray();

            int targetIndex = -1;

            for (int i = 0; i < lines.Count(); i++)
            {
                string[] info = lines[i].Split('|');

                if (int.Parse(info[0]) == item.Id)
                {
                    targetIndex = i;
                    break;
                }
            }

            if (targetIndex != -1)
            {
                string[] info = lines[targetIndex].Split('|');
                lines[targetIndex] = $"{item.Id}|{item.GuestUsername}|{item.HostUsername}|{item.ApartmentId}|{item.Grade}|{item.Text}|{item.IsAllowed}";
                File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/comments.txt"), lines);
            }
        }

        public Comment GetComment(int id)
        {
            var comments = GetComments();

            foreach (var comment in comments)
            {
                if (comment.Id == id)
                {
                    return comment;
                }
            }

            return null;
        }

        public bool AddComment(Comment commentToAdd)
        {
            if (commentToAdd == null)
            {
                return false;
            }


            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/comments.txt"));
            int id = SetId(lines);

            using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath(@"~/App_Data/comments.txt"), true))
            {
                writer.WriteLine($"{id}|{commentToAdd.GuestUsername}|{commentToAdd.HostUsername}|{commentToAdd.ApartmentId}|{commentToAdd.Grade}|{commentToAdd.Text}|{commentToAdd.IsAllowed}");
                return true;
            }
        }

        public List<Comment> GetComments()
        {
            List<Comment> comments = new List<Comment>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/comments.txt"));

            foreach (string line in lines)
            {
                string[] commentInfo = line.Split('|');

                Comment comment = new Comment()
                {
                    Id = int.Parse(commentInfo[0]),
                    GuestUsername = commentInfo[1],
                    HostUsername = commentInfo[2],
                    ApartmentId = int.Parse(commentInfo[3]),
                    Grade = double.Parse(commentInfo[4]),
                    Text = commentInfo[5],
                    IsAllowed = (commentInfo[6].ToLower() == "false") ? false : true
                };

                comments.Add(comment);
            }

            return comments;
        }
        #endregion

        #region Amenity
        public List<Amenity> GetAmenities()
        {
            List<Amenity> amenities = new List<Amenity>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/amenities.txt"));

            foreach (string line in lines)
            {
                string[] amenityInfo = line.Split('|');

                if (amenityInfo[2] == "False")
                {
                    Amenity amenity = new Amenity()
                    {
                        Id = int.Parse(amenityInfo[0]),
                        Name = amenityInfo[1]
                    };
                    amenities.Add(amenity);
                }
            }

            return amenities;
        }

        public bool AddAmenity(Amenity amenityToAdd)
        {
            if (amenityToAdd == null)
            {
                return false;
            }
            else if (AmenityExists(amenityToAdd.Name))
            {
                return false;
            }

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/amenities.txt"));
            int id = SetId(lines);

            using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath(@"~/App_Data/amenities.txt"), true))
            {
                writer.WriteLine($"{id}|{amenityToAdd.Name}|{amenityToAdd.IsDeleted}");
                return true;
            }
        }



        public bool AmenityExists(string name)
        {
            List<Amenity> amenities = GetAmenities();

            if (amenities == null)
            {
                return false;
            }
            
            foreach (var amenity in amenities)
            {
                if (amenity.Name == name)
                {
                    return true;
                }
            }

            return false;
        }

        public bool DeleteAmenity(string name)
        {
            
            if (!AmenityExists(name))
            {
                return false;
            }

            string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/amenities.txt")).ToArray();

            int deleteIndex = -1;

            for (int i = 0; i < lines.Count(); i++)
            {
                string[] amenityInfo = lines[i].Split('|');

                if (amenityInfo[2] == "False" && amenityInfo[1].ToLower() == name.ToLower())
                {
                    deleteIndex = i;
                    break;
                }
            }

            if (deleteIndex != -1)
            {
                string[] amenityInfo = lines[deleteIndex].Split('|');
                amenityInfo[2] = "True";
                lines[deleteIndex] = $"{amenityInfo[0]}|{amenityInfo[1]}|{amenityInfo[2]}";
                File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/amenities.txt"), lines);
            }

            return true;
        }
        #endregion

        #region Reservation
        public void UpdateReservation(Reservation reservation)
        {
            string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/reservations.txt")).ToArray();

            int targetIndex = -1;

            for (int i = 0; i < lines.Count(); i++)
            {
                string[] info = lines[i].Split('|');

                if (int.Parse(info[0]) == reservation.Id)
                {
                    targetIndex = i;
                    break;
                }
            }

            if (targetIndex != -1)
            {
                string[] info = lines[targetIndex].Split('|');
                lines[targetIndex] = $"{info[0]}|{info[1]}|{info[2]}|{info[3]}|{reservation.StartDate}|{reservation.Days}|{reservation.Status}|{reservation.TotalPrice}|{reservation.IsDeleted}";
                File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/reservations.txt"), lines);
            }
        }

        public Reservation GetReservation(int id)
        {
            List<Reservation> reservations = GetReservations();

            foreach (var reservation in reservations)
            {
                if (reservation.Id == id)
                {
                    return reservation;
                }
            }

            return null;
        }

        public void AddReservation(Reservation reservation, int apartmentId, string hostUsername, string guestUsername)
        {
            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/reservations.txt"));
            int id = SetId(lines);

            using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath(@"~/App_Data/reservations.txt"), true))
            {
                writer.WriteLine($"{id}|{apartmentId}|{hostUsername}|{guestUsername}|{reservation.StartDate}|{reservation.Days}|{reservation.Status}|{reservation.TotalPrice}|{reservation.IsDeleted}");
            }
        }

        public List<Reservation> GetReservations()
        {
            List<Reservation> reservations = new List<Reservation>();

            var lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/reservations.txt"));

            if (lines.Count() == 0)
            {
                return null;
            }

            foreach (string line in lines)
            {
                string[] info = line.Split('|');
                Reservation item = new Reservation()
                {
                    Id = int.Parse(info[0]),
                    ApartmentId = int.Parse(info[1]),
                    HostUsername = info[2],
                    GuestUsername = info[3],
                    StartDate = info[4],
                    Days = int.Parse(info[5]),
                    Status = info[6],
                    TotalPrice = double.Parse(info[7]),
                    IsDeleted = (info[8].ToLower() == "false") ? false : true
                };

                if (!item.IsDeleted)
                {
                    reservations.Add(item);
                }
            }

            return reservations;
        }
        #endregion

        #region Shared
        public bool EditUser(EditAccountViewModel userInfo)
        {
            if (!UserExists(userInfo.Username))
            {
                return false;
            } 

            if (GetUSerRole(userInfo.Username).ToLower() == "admin")
            {
                Admin user = GetAdmin(userInfo.Username);

                if (user != null)
                {
                    string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/admins.txt")).ToArray();

                    int targetIndex = -1;

                    for (int i = 0; i < lines.Count(); i++)
                    {
                        string[] info = lines[i].Split('|');

                        if (info[1] == userInfo.Username)
                        {
                            targetIndex = i;
                            break;
                        }
                    }

                    lines[targetIndex] = $"{user.Id}|{userInfo.Username}|{userInfo.Password}|{userInfo.Name}|{userInfo.Lastname}|{userInfo.Gender}|{user.IsDeleted}";
                    File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/admins.txt"), lines);
                }
            }
            else if (GetUSerRole(userInfo.Username).ToLower() == "host")
            {
                Host user = GetHost(userInfo.Username);

                if (user != null)
                {
                    string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/hosts.txt")).ToArray();

                    int targetIndex = -1;

                    for (int i = 0; i < lines.Count(); i++)
                    {
                        string[] info = lines[i].Split('|');

                        if (info[1] == userInfo.Username)
                        {
                            targetIndex = i;
                            break;
                        }
                    }

                    lines[targetIndex] = $"{user.Id}|{userInfo.Username}|{userInfo.Password}|{userInfo.Name}|{userInfo.Lastname}|{userInfo.Gender}|{user.IsDeleted}";
                    File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/hosts.txt"), lines);
                }
            }
            else if (GetUSerRole(userInfo.Username).ToLower() == "guest")
            {
                Guest user = GetGuest(userInfo.Username);

                if (user != null)
                {
                    string[] lines = File.ReadLines(HostingEnvironment.MapPath(@"~/App_Data/guests.txt")).ToArray();

                    int targetIndex = -1;

                    for (int i = 0; i < lines.Count(); i++)
                    {
                        string[] info = lines[i].Split('|');

                        if (info[1] == userInfo.Username)
                        {
                            targetIndex = i;
                            break;
                        }
                    }

                    lines[targetIndex] = $"{user.Id}|{userInfo.Username}|{userInfo.Password}|{userInfo.Name}|{userInfo.Lastname}|{userInfo.Gender}|{user.IsDeleted}";
                    File.WriteAllLines(HostingEnvironment.MapPath(@"~/App_Data/guests.txt"), lines);
                }
            }

            return true;
        }

        public User UserInfo(string username)
        {
            if (!UserExists(username))
            {
                return null;
            }

            List<Admin> admins = GetAdmins();
            List<Guest> guests = GetGuests();
            List<Host> hosts = GetHosts();

            foreach (var admin in admins)
            {
                if (admin.Username == username)
                {
                    return admin;
                }
            }

            foreach (var host in hosts)
            {
                if (host.Username == username)
                {
                    return host;
                }
            }

            foreach (var guest in guests)
            {
                if (guest.Username == username)
                {
                    return guest;
                }
            }

            return null;
        }
        private bool UserExists(string username)
        {
            List<Admin> admins = GetAdmins();
            List<Guest> guests = GetGuests();
            List<Host> hosts = GetHosts();

            foreach (var admin in admins)
            {
                if (admin.Username == username)
                {
                    return true;
                }
            }

            foreach (var host in hosts)
            {
                if (host.Username == username)
                {
                    return true;
                }
            }

            foreach (var guest in guests)
            {
                if (guest.Username == username)
                {
                    return true;
                }
            }

            return false;
        }

        private int SetId(IEnumerable<string> lines)
        {
            int id = 1;

            if (lines == null)
            {
                return id;
            }

            return lines.Count() + 1;
        }

        public string GetUSerRole(string username)
        {
            List<Admin> admins = GetAdmins();
            List<Guest> guests = GetGuests();
            List<Host> hosts = GetHosts();

            foreach (var admin in admins)
            {
                if (admin.Username == username)
                {
                    return "ADMIN";
                }
            }

            foreach (var host in hosts)
            {
                if (host.Username == username)
                {
                    return "HOST";
                }
            }

            foreach (var guest in guests)
            {
                if (guest.Username == username)
                {
                    return "GUEST";
                }
            }

            return null;
        }
        #endregion

        #region User
        public User GetUser(string username)
        {
            User user = null;

            if (GetUSerRole(username).ToLower() == "guest")
            {
                user = GetGuest(username);
            }
            else if (GetUSerRole(username).ToLower() == "host")
            {
                user = GetHost(username);
            }

            return user;
        }

        public List<User> GetUsers()
        {
            var guests = GetGuests();
            var hosts = GetHosts();

            List<User> users = new List<User>();

            foreach (var guest in guests)
            {
                users.Add(guest);
            }

            foreach (var host in hosts)
            {
                users.Add(host);
            }

            return users;
        }

        #endregion User
    }
}