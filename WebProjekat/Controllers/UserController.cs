﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;
using WebProjekat.ViewModel;

namespace WebProjekat.Controllers
{
    public class UserController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        // GET: User
        public ActionResult Show()
        {
            try
            {
                string accountType = "unknown";

                if (Session["User"] != null)
                {
                    User user = (User)Session["User"];
                    accountType = user.Type;

                    if (accountType.ToLower() != "admin")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

                ShowUserViewModel viewModel = new ShowUserViewModel()
                {
                    Users = repository.GetUsers(),
                    AccountType = accountType
                };

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Edit(string username)
        {
            try
            {
                if (Session["User"] != null)
                {
                    User user = (User)Session["User"];

                    if (user.Type.ToLower() != "admin")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

                User userToEdit = repository.GetUser(username);
                EditAccountViewModel viewModel = new EditAccountViewModel()
                {
                    Username = userToEdit.Username,
                    Name = userToEdit.Name,
                    Lastname = userToEdit.LastName,
                    Gender = userToEdit.Gender
                };

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult PostEdit(EditAccountViewModel userInfo)
        {
            try
            {
                if (userInfo.Username == null || userInfo.Name == null || userInfo.Lastname == null || userInfo.Gender == null)
                {
                    TempData["Error"] = "You must fill all fields!";
                    return RedirectToAction("Edit");
                }

                if (repository.EditUser(userInfo))
                {
                    TempData["Message"] = "Success!";
                    return RedirectToAction("Edit");
                }
                else
                {
                    TempData["Error"] = "There was an error!";
                    return RedirectToAction("Edit");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "There was an error!";
                return RedirectToAction("Edit");
            }
        }

        public ActionResult Search(UserSearch info)
        {
            try
            {
                var users = repository.GetUsers();
                ShowUserViewModel viewModel = new ShowUserViewModel()
                {
                    Users = new List<User>()
                };

                if (info.Username != null)
                {
                    foreach (var user in users)
                    {
                        if (user.Username == info.Username && user.Type.ToLower() == info.Role.ToLower() && user.Gender.ToLower() == info.Gender.ToLower())
                        {
                            viewModel.Users.Add(user);
                        }
                    }
                }
                else
                {
                    foreach (var user in users)
                    {
                        if (user.Type.ToLower() == info.Role.ToLower() && user.Gender.ToLower() == info.Gender.ToLower())
                        {
                            viewModel.Users.Add(user);
                        }
                    }
                }

                return View("Show", viewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}