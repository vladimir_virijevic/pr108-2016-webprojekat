﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;
using WebProjekat.ViewModel;

namespace WebProjekat.Controllers
{
    public class ApartmentController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        public ActionResult Create()
        {
            CreateApartmentViewModel viewModel = new CreateApartmentViewModel()
            {
                Amenities = repository.GetAmenities()
            };

            return View("Create", viewModel);
        }

        public ActionResult Show(int id)
        {
            try
            {
                Apartment apartment = repository.GetApartment(id);

                var comments = repository.GetComments();

                ShowApartmentViewModel viewModel = new ShowApartmentViewModel()
                {
                    Apartment = apartment,
                    UserType = "",
                    Comments = new List<Comment>()
                };

                foreach (var comment in comments)
                {
                    if (comment.ApartmentId == apartment.Id)
                    {
                        viewModel.Comments.Add(comment);
                    }
                }

                if (apartment != null)
                {
                    if (Session["User"] != null)
                    {
                        // Provera se da li user ima pravo da vidi trazeni stan
                        User user = (User)Session["User"];

                        viewModel.UserType = user.Type;

                        if (user.Type.ToLower() == "host")
                        {
                            if (user.Username == apartment.Host)
                            {
                                return View(viewModel);
                            }
                        }
                        else if (user.Type.ToLower() == "guest")
                        {
                            if (apartment.ApartmentStatus.ToLower() == "active")
                            {
                                return View(viewModel);
                            }
                        }
                        else
                        {
                            return View(viewModel);
                        }
                    }
                    else
                    {
                        return View(viewModel);
                    }
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Activate(int id)
        {
            if (Session["User"] != null)
            {
                User user = (User)Session["User"];
                Apartment apartmentToActivate = repository.GetApartment(id);

                if (apartmentToActivate != null)
                {
                    if (user.Type.ToLower() == "host")
                    {
                        if (user.Username == apartmentToActivate.Host)
                        {
                            repository.ActivateApartment(id);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Delete(int id)
        {
            if (Session["User"] != null)
            {
                User user = (User)Session["User"];
                Apartment apartmentToActivate = repository.GetApartment(id);

                if (apartmentToActivate != null)
                {
                    if (user.Type.ToLower() == "host")
                    {
                        if (user.Username == apartmentToActivate.Host)
                        {
                            DeleteApartmentReservations(id);
                            repository.DeleteApartment(id);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else if (user.Type.ToLower() == "admin")
                    {
                        DeleteApartmentReservations(id);
                        repository.DeleteApartment(id);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return RedirectToAction("Index", "Home");
        }

        private void DeleteApartmentReservations(int apartmentId)
        {
            var reservations = repository.GetReservations();

            foreach (var reservation in reservations)
            {
                if (reservation.ApartmentId == apartmentId)
                {
                    reservation.IsDeleted = true;
                    repository.UpdateReservation(reservation);
                }
            }
        }


        [HttpPost]
        public ActionResult PostCreate(ApartmentInput apartment)
        {
            if (apartment.Type == null || apartment.RoomNumber == 0 || apartment.GuestCapacity == 0 || apartment.Dates == null || apartment.Price == 0 || apartment.CheckInTime == null || apartment.CheckOutTime == null || apartment.Address == null || apartment.StreetNumber == null || apartment.PostalCode == null)
            {
                TempData["Error"] = "You must enter all fields correctly";
                return RedirectToAction("Create");
            }

            if (apartment.Amenities.Count() == 0)
            {
                if (TempData["Error"] != null)
                {
                    TempData["Error"] += ". You must choose amenities";
                }

                TempData["Error"] = "You must enter all fields";
                return RedirectToAction("Create");
            }

            if (apartment.Amenities.Count() == 0)
            {
                if (TempData["Error"] != null)
                {
                    TempData["Error"] += ". You must choose atleast one photo";
                }

                TempData["Error"] = "You must enter all fields";
                return RedirectToAction("Create");
            }

            try
            {
                if (apartment.Latitude == null || apartment.Longitude == null)
                {
                    apartment.Latitude = "0";
                    apartment.Longitude = "0";
                }

                repository.AddApartment(apartment);

                foreach (var file in apartment.Files)
                {
                    if (file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content"), fileName);
                        file.SaveAs(path);
                    }
                }

                TempData["Message"] = "Apartment created succesfully";
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "There was an error";
                return RedirectToAction("Create");
            }
        }

        public ActionResult Rent(int id)
        {
            try
            {
                Apartment apartment = repository.GetApartment(id);

                if (apartment == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                RentApartmentViewModel viewModel = new RentApartmentViewModel()
                {
                    Id = id,
                    AvailableDates = apartment.DatesAvailability,
                    RentDate = "",
                    Days = 0
                };

                return View(viewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult PostRent(RentApartmentViewModel rentInfo)
        {
            try
            {
                Apartment apartment = repository.GetApartment(rentInfo.Id);

                if (apartment == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                if (DatesAreValid(apartment.DatesAvailability, rentInfo.RentDate, rentInfo.Days))
                {
                    string newDates = RemoveRentDates(apartment, rentInfo.RentDate, rentInfo.Days);
                    apartment.DatesAvailability = newDates;

                    repository.UpdateApartment(apartment);

                    Reservation reservation = new Reservation()
                    {
                        StartDate = rentInfo.RentDate,
                        Days = rentInfo.Days,
                        Status = "CREATED",
                        IsDeleted = false,
                        TotalPrice = apartment.Price * rentInfo.Days
                    };

                    string guestUsername = ((User)Session["User"]).Username;

                    repository.AddReservation(reservation, apartment.Id, apartment.Host, guestUsername);

                    TempData["Message"] = "Apartmant rented successfully";
                    return RedirectToAction("Rent", new { id = apartment.Id});
                }
                else
                {
                    TempData["Error"] = "Apartmant is not available";
                    return RedirectToAction("Rent", new { id = apartment.Id });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private bool DatesAreValid(string available, string rent, int days)
        {
            string availableDates = Regex.Replace(available, @"\s+", "");

            DateTime startDate = DateTime.Parse(rent);
            DateTime endDate = startDate.AddDays(days - 1);

            // Napravi listu datuma koje je izabrao gost
            List<DateTime> rentDates = new List<DateTime>();

            for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
            {
                rentDates.Add(i);
            }


            foreach (DateTime date in rentDates)
            {
                string stringDate = date.ToString("MM/dd/yyyy");

                if (!availableDates.Contains(stringDate))
                {
                    return false;
                }
            }

            return true;
        }

        private string RemoveRentDates(Apartment apartment, string rent, int days) 
        {
            DateTime startDate = DateTime.Parse(rent);
            DateTime endDate = startDate.AddDays(days - 1);

            // Napravi listu datuma koje je izabrao gost
            List<DateTime> rentDates = new List<DateTime>();

            for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
            {
                rentDates.Add(i);
            }

            List<string> availableDates = Regex.Replace(apartment.DatesAvailability, @"\s+", "").Split(',').ToList();

            string result = "";

            foreach (DateTime date in rentDates)
            {
                string dateAsString = date.ToString("MM/dd/yyyy");

                if (availableDates.Contains(dateAsString))
                {
                    availableDates.Remove(dateAsString);
                }
            }

            foreach (string date in availableDates)
            {
                result += date + ", ";
            }

            if (availableDates.Count > 0)
            {
                result = result.Substring(0, result.Length - 2);
            }

            return result;
        }

        public ActionResult Edit(int id)
        {
            try
            {
                if (Session["User"] != null)
                {
                    User user = (User)Session["User"];

                    if (user.Type.ToLower() == "host")
                    {
                        Apartment apartment = repository.GetApartment(id);

                        if (user.Username.ToLower() != apartment.Host.ToLower())
                        {
                            return RedirectToAction("Index", "Home");
                        }

                        EditApartmentViewModel viewModel = new EditApartmentViewModel()
                        {
                            Apartment = apartment,
                            Amenities = repository.GetAmenities()
                        };

                        return View(viewModel);
                    }
                    else if (user.Type.ToLower() == "admin")
                    {
                        Apartment apartment = repository.GetApartment(id);

                        EditApartmentViewModel viewModel = new EditApartmentViewModel()
                        {
                            Apartment = apartment,
                            Amenities = repository.GetAmenities()
                        };

                        return View(viewModel);
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult PostEdit(ApartmentInput apartment)
        {
            if (apartment.Type == null || apartment.RoomNumber == 0 || apartment.GuestCapacity == 0 || apartment.Dates == null || apartment.Price == 0 || apartment.CheckInTime == null || apartment.CheckOutTime == null || apartment.Address == null || apartment.StreetNumber == null || apartment.PostalCode == null)
            {
                TempData["Error"] = "You must enter all fields correctly";
                return RedirectToAction("Create");
            }

            if (apartment.Amenities == null)
            {
                if (TempData["Error"] != null)
                {
                    TempData["Error"] += ". You must choose amenities";
                }

                TempData["Error"] = "You must enter all fields";
                return RedirectToAction("Edit", new { id = apartment.Id});
            }

            if (apartment.Amenities.Count() == 0)
            {
                if (TempData["Error"] != null)
                {
                    TempData["Error"] += ". You must choose amenities";
                }

                TempData["Error"] = "You must enter all fields";
                return RedirectToAction("Edit" , new { id = apartment.Id });
            }

            try
            {
                string amenities = "";

                foreach (string amenity in apartment.Amenities)
                {
                    amenities += amenity + '_';
                }

                amenities = amenities.Remove(amenities.Length - 1);

                Apartment apartmentToUpdate = new Apartment()
                {
                    Id = apartment.Id,
                    Type = apartment.Type,
                    RoomNumber = apartment.RoomNumber,
                    GuestCapacity = apartment.GuestCapacity,
                    Dates = apartment.Dates,
                    DatesAvailability = apartment.Dates,
                    Pictures = apartment.Pictures,
                    Price = apartment.Price,
                    CheckInTime = apartment.CheckInTime,
                    CheckOutTime = apartment.CheckOutTime,
                    ApartmentStatus = apartment.ApartmentStatus,
                    IsDeleted = false,
                    Amenities = amenities,
                    Host = apartment.Host,
                    Guest = apartment.Guest,
                    Location = new Location()
                    {
                        Address = apartment.Address,
                        StreetNumber = apartment.StreetNumber,
                        PostalCode = apartment.PostalCode,
                        Lat = double.Parse(apartment.Latitude),
                        Long = double.Parse(apartment.Longitude)
                    }
                };

                if (repository.UpdateApartment(apartmentToUpdate))
                {
                    TempData["Message"] = "Apartment updated successufully!";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Error"] = "There was an error!!";
                    return RedirectToAction("Edit");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}