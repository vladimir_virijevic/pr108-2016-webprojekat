﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;
using WebProjekat.ViewModel;

namespace WebProjekat.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        public ActionResult Index()
        {
            HomeViewModel homeViewModel = new HomeViewModel();

            homeViewModel.UserType = "unknown";

            if (Session["User"] != null)
            {
                User user = (User)Session["User"];
                homeViewModel.UserIsLoggedIn = true;
                homeViewModel.Username = user.Username;
                homeViewModel.UserType = user.Type;
            }
            else
            {
                homeViewModel.UserIsLoggedIn = false;
            }

            try
            {
                homeViewModel.Apartments = repository.GetApartments();
            }
            catch (Exception ex)
            {
                
            }

            return View(homeViewModel);
        }

        [HttpPost]
        public ActionResult Search(ApartmentSearch searchInfo)
        {
            // Verifikacija
            if (searchInfo.StartDate == null && searchInfo.EndDate == null && searchInfo.Location == null && searchInfo.MinPrice == 0 && searchInfo.MaxPrice == 0 && searchInfo.MinRoomNumber == 0 && searchInfo.MaxRoomNumber == 0 && searchInfo.GuestCapacity == 0)
            {
                TempData["Error"] = "You must choose atleast one search category";
                return RedirectToAction("Index", "Home");
            }

            // Provera da li su oba datuma popunjena
            if (searchInfo.StartDate != null)
            {
                if (searchInfo.EndDate == null)
                {
                    TempData["Error"] = "You must enter both start and end date";
                    return RedirectToAction("Index", "Home");
                }
            }
            if (searchInfo.EndDate != null)
            {
                if (searchInfo.StartDate == null)
                {
                    TempData["Error"] = "You must enter both start and end date";
                    return RedirectToAction("Index", "Home");
                }
            }

            // Provera da li su oba polja za sobe popunjena
            if (searchInfo.MinRoomNumber != 0)
            {
                if (searchInfo.MaxRoomNumber == 0)
                {
                    TempData["Error"] = "You must enter both min and max room number";
                    return RedirectToAction("Index", "Home");
                }
                else if (searchInfo.MinRoomNumber > searchInfo.MaxRoomNumber)
                {
                    TempData["Error"] = "Maximum room number must be bigger then minimum room number";
                    return RedirectToAction("Index", "Home");
                }
            }
            if (searchInfo.MaxRoomNumber != 0)
            {
                if (searchInfo.MinRoomNumber == 0)
                {
                    TempData["Error"] = "You must enter both min and max room number";
                    return RedirectToAction("Index", "Home");
                }
                else if (searchInfo.MinRoomNumber > searchInfo.MaxRoomNumber)
                {
                    TempData["Error"] = "Maximum room number must be bigger then minimum room number";
                    return RedirectToAction("Index", "Home");
                }
            }

            // Provera da li su oba polja za cene popunjena
            if (searchInfo.MinPrice != 0)
            {
                if (searchInfo.MaxPrice == 0)
                {
                    TempData["Error"] = "You must enter both min and max price";
                    return RedirectToAction("Index", "Home");
                }
                else if (searchInfo.MinPrice > searchInfo.MaxPrice)
                {
                    TempData["Error"] = "Maximum price must be bigger then minimum price";
                    return RedirectToAction("Index", "Home");
                }
            }
            if (searchInfo.MaxPrice != 0)
            {
                if (searchInfo.MinPrice == 0)
                {
                    TempData["Error"] = "You must enter both min and max price";
                    return RedirectToAction("Index", "Home");
                }
                else if (searchInfo.MinPrice > searchInfo.MaxPrice)
                {
                    TempData["Error"] = "Maximum price must be bigger then minimum price";
                    return RedirectToAction("Index", "Home");
                }
            }

            

            try
            {
                List<Apartment> searchResults = null;
                List<Apartment> apartments = repository.GetApartments();
                if (apartments != null)
                {
                    searchResults = apartments;
                }
                else
                {
                    searchResults = new List<Apartment>();
                }


                // Pretrage lokacije
                if (searchInfo.Location != null)
                {
                    for (int i = 0; i < searchResults.Count(); i++)
                    {
                        if (!searchResults[i].Location.Address.ToLower().Contains(searchInfo.Location.ToLower()))
                        {
                            searchResults[i] = null;
                        }
                    }
                    
                }

                // Pretraga po broju soba
                if (searchInfo.MinRoomNumber != 0 || searchInfo.MaxRoomNumber != 0)
                {
                    for (int i = 0; i < searchResults.Count(); i++)
                    {
                        if (searchResults[i] != null)
                        {
                            if (searchResults[i].RoomNumber >= searchInfo.MinRoomNumber && searchResults[i].RoomNumber <= searchInfo.MaxRoomNumber)
                            {

                            }
                            else
                            {
                                searchResults[i] = null;
                            }
                        }
                    }
                }

                // Pretraga po ceni
                if (searchInfo.MinPrice != 0 || searchInfo.MinPrice != 0)
                {
                    for (int i = 0; i < searchResults.Count(); i++)
                    {
                        if (searchResults[i] != null)
                        {
                            if (searchResults[i].Price >= searchInfo.MinPrice && searchResults[i].Price <= searchInfo.MaxPrice)
                            {

                            }
                            else
                            {
                                searchResults[i] = null;
                            }
                        }
                    }
                }

                // Pretraga po broju gostiju
                if (searchInfo.GuestCapacity != 0)
                {
                    for (int i = 0; i < searchResults.Count(); i++)
                    {
                        if (searchResults[i] != null)
                        {
                            if (searchResults[i].GuestCapacity >= searchInfo.GuestCapacity)
                            {

                            }
                            else
                            {
                                searchResults[i] = null;
                            }
                        }
                    }
                }

                // Pretraga po datumu
                if (searchInfo.StartDate != null && searchInfo.EndDate != null)
                {
                    DateTime startDate = DateTime.Parse(searchInfo.StartDate);
                    DateTime endDate = DateTime.Parse(searchInfo.EndDate);

                    if (startDate <= endDate)
                    {
                        List<string> searchDates = new List<string>();

                        for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        {
                            searchDates.Add(date.ToString("MM/dd/yyyy"));
                        }

                        for (int i = 0; i < searchResults.Count(); i++)
                        {
                            if (searchResults[i] == null)
                            {
                                continue;
                            }

                            bool passedSearch = true;
                            string[] availableDates = searchResults[i].DatesAvailability.Replace(" ", "").Split(',');

                            foreach (string date in searchDates)
                            {
                                if (!availableDates.Contains(date))
                                {
                                    passedSearch = false;
                                }
                            }

                            if (passedSearch == false)
                            {
                                searchResults[i] = null;
                            }
                        }
                    }
                    else
                    {
                        TempData["Error"] = "End date must be bigger then start date";
                        return RedirectToAction("Index", "Home");
                    }
                }

                HomeViewModel homeViewModel = new HomeViewModel();

                homeViewModel.UserType = "unknown";
                homeViewModel.Apartments = new List<Apartment>();

                if (Session["User"] != null)
                {
                    User user = (User)Session["User"];
                    homeViewModel.UserIsLoggedIn = true;
                    homeViewModel.Username = user.Username;
                    homeViewModel.UserType = user.Type;
                }
                else
                {
                    homeViewModel.UserIsLoggedIn = false;
                }

                foreach (var apartment in searchResults)
                {
                    if (apartment != null)
                    {
                        homeViewModel.Apartments.Add(apartment);
                    }
                }

                return View("Index", homeViewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Sort(SortInput sortInfo)
        {
            try
            {
                HomeViewModel viewModel = new HomeViewModel()
                {
                    Apartments = new List<Apartment>()
                };

                viewModel.UserType = "unknown";

                if (Session["User"] != null)
                {
                    User user = (User)Session["User"];
                    viewModel.UserIsLoggedIn = true;
                    viewModel.Username = user.Username;
                    viewModel.UserType = user.Type;
                }
                else
                {
                    viewModel.UserIsLoggedIn = false;
                }

                viewModel.Apartments = repository.GetApartments();

                if (viewModel.Apartments == null)
                {
                    viewModel.Apartments = new List<Apartment>();
                }
                else
                {
                    if (sortInfo.Order.ToLower() == "asc")
                    {
                        viewModel.Apartments = viewModel.Apartments.OrderBy(o => o.Price).ToList();
                    }
                    else
                    {
                        viewModel.Apartments = viewModel.Apartments.OrderByDescending(o => o.Price).ToList();
                    }
                }

                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}