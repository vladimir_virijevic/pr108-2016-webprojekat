﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;

namespace WebProjekat.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateHost()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateHost(Host host)
        {
            if (host.Username == null || host.Name == null || host.LastName == null || host.Password == null)
            {
                TempData["Error"] = "You must fill all host informations!";
                return View();
            }

            bool createResult = repository.AddHost(host);

            if (!createResult)
            {
                TempData["Error"] = "User already exists!";
                return View();
            }

            TempData["Message"] = "Host Created Successfuly";
            return View();
        }
    }
}