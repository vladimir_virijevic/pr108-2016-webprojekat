﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;

namespace WebProjekat.Controllers
{
    public class AmenitiesController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        public ActionResult GetCreate()
        {
            List<Amenity> amenities = repository.GetAmenities();

            return View("Create", amenities);
        }

        [HttpPost]
        public ActionResult Create(Amenity amenity)
        {
            if (amenity.Name == null)
            {
                TempData["Error"] = "You must enter amenity name";
                return RedirectToAction("GetCreate");
            }

            bool addResult = repository.AddAmenity(amenity);

            if (addResult)
            {
                TempData["Message"] = "Amenity created successfully";
                return RedirectToAction("GetCreate");
            }
            else
            {
                TempData["Error"] = "Amenity already exists";
                return RedirectToAction("GetCreate");
            }
        }

        public ActionResult Delete(string name)
        {
            if (name == "")
            {
                TempData["Error"] = "There was an error!";
            }

            bool result = repository.DeleteAmenity(name);

            if (result)
            {
                TempData["Message"] = "Amenity deleted successfully";
            }
            else
            {
                TempData["Error"] = "There was an error!";
            }

            return RedirectToAction("GetCreate");
        }
    }
}