﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;
using WebProjekat.ViewModel;

namespace WebProjekat.Controllers
{
    public class CommentController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        public ActionResult Allow(int id)
        {
            try
            {
                Comment comment = repository.GetComment(id);

                comment.IsAllowed = true;

                repository.UpdateComment(comment);
                return RedirectToAction("Show", "Comment");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Show()
        {
            try
            {
                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                User user = (User)Session["User"];
                ShowCommentViewModel viewModel = new ShowCommentViewModel()
                {
                    Comments = new List<Comment>(),
                    UserType = ""
                };

                if (repository.GetUSerRole(user.Username).ToLower() == "guest")
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (repository.GetUSerRole(user.Username).ToLower() == "admin")
                {
                    viewModel.UserType = "admin";
                    viewModel.Comments = repository.GetComments();
                    return View(viewModel);
                }
                else if (repository.GetUSerRole(user.Username).ToLower() == "host")
                {
                    var allComments = repository.GetComments();

                    foreach (var comment in allComments)
                    {
                        if (comment.HostUsername == user.Username)
                        {
                            viewModel.Comments.Add(comment);
                        }
                    }
                    viewModel.UserType = "host";

                    return View(viewModel);
                }

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Add(Comment comment)
        {
            try
            {
                if (comment == null)
                {
                    TempData["Error"] = "You must enter all fields!";
                    return RedirectToAction("Show", "Apartment", new { id = comment.ApartmentId });
                }

                if (comment.Grade == 0 || comment.Text == null)
                {
                    TempData["Error"] = "You must enter all fields!";
                    return RedirectToAction("Show", "Apartment", new { id = comment.ApartmentId });
                }

                if (comment.Grade < 0 || comment.Grade > 10)
                {
                    TempData["Error"] = "Apartment grade must be between 1 and 10";
                    return RedirectToAction("Show", "Apartment", new { id = comment.ApartmentId });
                }

                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                Guest guest = (Guest)Session["User"];

                var reservations = repository.GetReservations();

                

                Apartment apartment = repository.GetApartment(comment.ApartmentId);
                comment.GuestUsername = guest.Username;
                comment.HostUsername = apartment.Host;

                bool permission = false;

                foreach (var reservation in reservations)
                {
                    if (reservation.GuestUsername == comment.GuestUsername && reservation.HostUsername == comment.HostUsername)
                    {
                        if (reservation.Status.ToLower() == "rejected" || reservation.Status.ToLower() == "finished")
                        {
                            permission = true;
                            break;
                        }
                    }
                }

                if (permission)
                {
                    repository.AddComment(comment);
                    TempData["Message"] = "Comment addded succesfully";
                }
                else
                {
                    TempData["Error"] = "You do not have permission to add comment";
                }

                return RedirectToAction("Show", "Apartment", new { id = comment.ApartmentId });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }

        }
    }
}