﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;
using WebProjekat.ViewModel;

namespace WebProjekat.Controllers
{
    public class ReservationController : Controller
    {
        private ApplicationRepository repository = new ApplicationRepository();

        // GET: Reservation
        public ActionResult Index()
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Guest user = (Guest)Session["User"];
            ReservationViewModel viewModel = new ReservationViewModel()
            {
                Reservations = new List<Reservation>(),
                User = user
            };

            viewModel.Reservations = repository.GetReservations();

            if (viewModel.Reservations == null)
            {
                viewModel.Reservations = new List<Reservation>();
            }


            if (user.Type.ToLower() == "host")
            {
                List<Reservation> tempList = new List<Reservation>();

                for (int i = 0; i < viewModel.Reservations.Count(); i++)
                {
                    if (viewModel.Reservations[i].HostUsername == user.Username)
                    {
                        tempList.Add(viewModel.Reservations[i]);
                    }
                }

                viewModel.Reservations = tempList;
            }
            else if (user.Type.ToLower() == "guest")
            {
                List<Reservation> tempList = new List<Reservation>();

                for (int i = 0; i < viewModel.Reservations.Count(); i++)
                {
                    if (viewModel.Reservations[i].GuestUsername == user.Username)
                    {
                        tempList.Add(viewModel.Reservations[i]);
                    }
                }
                viewModel.Reservations = tempList;
            }

            return View(viewModel);
        }

        public ActionResult Accept(int id)
        {
            try
            {
                Reservation reservation = repository.GetReservation(id);

                if (reservation == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                Guest user = (Guest)Session["User"];

                if (user.Username != reservation.HostUsername)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                reservation.Status = "ACCEPTED";
                repository.UpdateReservation(reservation);

                return RedirectToAction("Index", "Reservation");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Finish(int id)
        {
            try
            {
                Reservation reservation = repository.GetReservation(id);

                if (reservation == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                Guest user = (Guest)Session["User"];

                if (user.Username != reservation.HostUsername)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                // Provera da li je rezervacija istekla
                DateTime currentDate = DateTime.Now;
                DateTime startDate = DateTime.Parse(reservation.StartDate);
                DateTime endDate = startDate.AddDays(reservation.Days - 1);

                if (currentDate > endDate)
                {
                    reservation.Status = "FINISHED";
                    repository.UpdateReservation(reservation);
                }

                return RedirectToAction("Index", "Reservation");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Reject(int id)
        {
            try
            {
                Reservation reservation = repository.GetReservation(id);
                Apartment apartment = repository.GetApartment(reservation.ApartmentId);

                if (reservation == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }


                DateTime startDate = DateTime.Parse(reservation.StartDate);
                DateTime endDate = startDate.AddDays(reservation.Days - 1);

                // Napravi listu datuma koje je izabrao gost
                List<DateTime> rentDates = new List<DateTime>();

                for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
                {
                    rentDates.Add(i);
                }

                if (rentDates.Count > 0 && apartment.DatesAvailability != "")
                {
                    apartment.DatesAvailability += ",";
                }

                // Vrati sve datume u listu dostpunih datuma apartmana
                foreach (DateTime date in rentDates)
                {
                    string dateToAdd = date.ToString("MM/dd/yyyy");
                    apartment.DatesAvailability += dateToAdd + ",";
                }

                apartment.DatesAvailability = apartment.DatesAvailability.Remove(apartment.DatesAvailability.LastIndexOf(","), 1);
                repository.UpdateApartment(apartment);

                reservation.Status = "REJECTED";
                repository.UpdateReservation(reservation);

                return RedirectToAction("Index", "Reservation");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Cancell(int id)
        {
            try
            {
                Reservation reservation = repository.GetReservation(id);
                Apartment apartment = repository.GetApartment(reservation.ApartmentId);

                if (reservation == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }

                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Reservation");
                }


                DateTime startDate = DateTime.Parse(reservation.StartDate);
                DateTime endDate = startDate.AddDays(reservation.Days - 1);

                // Napravi listu datuma koje je izabrao gost
                List<DateTime> rentDates = new List<DateTime>();

                for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
                {
                    rentDates.Add(i);
                }

                if (rentDates.Count > 0 && apartment.DatesAvailability != "")
                {
                    apartment.DatesAvailability += ",";
                }

                // Vrati sve datume u listu dostpunih datuma apartmana
                foreach (DateTime date in rentDates)
                {
                    string dateToAdd = date.ToString("MM/dd/yyyy");
                    apartment.DatesAvailability += dateToAdd + ",";
                }

                apartment.DatesAvailability = apartment.DatesAvailability.Remove(apartment.DatesAvailability.LastIndexOf(","), 1);
                repository.UpdateApartment(apartment);

                reservation.Status = "CANCELLED";
                repository.UpdateReservation(reservation);

                return RedirectToAction("Index", "Reservation");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Search(ReservationsSerach searchInfo)
        {
            try
            {
                if (searchInfo.GuestUsername == null)
                {
                    TempData["Error"] = "You must enter guest username";
                    return RedirectToAction("Index", "Reservation");
                }

                var reservations = repository.GetReservations();

                if (Session["User"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                Guest user = (Guest)Session["User"];
                ReservationViewModel viewModel = new ReservationViewModel()
                {
                    Reservations = new List<Reservation>(),
                    User = user
                };

                foreach (var reservation in reservations)
                {
                    if (user.Username == reservation.HostUsername && reservation.GuestUsername == searchInfo.GuestUsername)
                    {
                        viewModel.Reservations.Add(reservation);
                    }
                }

                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}