﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProjekat.Models;
using WebProjekat.Repository;
using WebProjekat.Security;
using WebProjekat.ViewModel;

namespace WebProjekat.Controllers
{
    public class AccountController : Controller
    {
        private AccountSecurity accountSecurity = new AccountSecurity();
        private ApplicationRepository repository = new ApplicationRepository();

        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            if (username == "" || password == "")
            {
                TempData["Error"] = "You must fill all fields";
                return View();
            }

            if (!accountSecurity.LoginIsValid(username, password))
            {
                TempData["Error"] = "Wrong username/password";
                return View();
            }
            Guest user = new Guest()
            {
                Username = username,
                Password = password,
                Type = repository.GetUSerRole(username)
            };

            Session["User"] = user;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Guest guest)
        {
            if (guest.Username == null || guest.Name == null || guest.LastName == null || guest.Password == null)
            {
                TempData["Error"] = "You must fill all guest informations!";
                return View();
            }

            bool registerResult = repository.AddGuest(guest);

            if (!registerResult)
            {
                TempData["Error"] = "Username must be unique!";
                return View();
            }

            TempData["Message"] = "Guest Created Successfuly";
            return View();
        }

        public ActionResult Logout()
        {
            if (Session["User"] != null)
            {
                Session["User"] = null;

                return RedirectToAction("Login", "Account");
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit()
        {
            if (Session["User"] != null)
            {
                User user = repository.UserInfo(((User)Session["User"]).Username);

                EditAccountViewModel viewModel = new EditAccountViewModel()
                {
                    Username = user.Username,
                    Password = user.Password,
                    Gender = user.Gender,
                    Name = user.Name,
                    Lastname = user.LastName
                };

                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult PostEdit(EditAccountViewModel userInfo)
        {
            try
            {
                if (userInfo.Username == null || userInfo.Name == null || userInfo.Lastname == null || userInfo.Password == null || userInfo.Gender == null)
                {
                    TempData["Error"] = "You must fill all fields!";
                    return View();
                }

                if (repository.EditUser(userInfo))
                {
                    TempData["Message"] = "Success!";
                }

                return RedirectToAction("Edit");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}